/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class JavaButton {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example"); 
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);

        final JTextField txt = new JTextField(); 
        txt.setBounds(50, 50, 150, 20);
        frame.add(txt);

        JButton btnclick = new JButton("Click Here"); 
        btnclick.setBounds(50, 100, 95, 30);
        btnclick.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                txt.setText("Welcome to Javapoint."); 

            }

        });

        frame.add(btnclick);

    }
}
