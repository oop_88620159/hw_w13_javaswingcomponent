/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author acer
 */
public class JavaJProgressBar extends JFrame {

    JProgressBar pb;

    int i = 0, num = 0;

    JavaJProgressBar() {
        pb = new JProgressBar(0, 2000);
        pb.setBounds(40, 40, 160, 30);
        pb.setValue(0);
        pb.setStringPainted(true);
        add(pb);
        setSize(250, 150);
        setLayout(null);

    }

    public void iterate() {
        while (i <= 2000) {
            pb.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }

        }
    }

    public static void main(String[] args) {
        JavaJProgressBar jpb = new JavaJProgressBar();
        jpb.setVisible(true);
        jpb.iterate();

    }
}
