/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author acer
 */
public class JavaJEditorPane {

    JFrame myframe = null;

    public static void main(String[] args) {
        (new JavaJEditorPane()).test();
    }

    private void test() {
        myframe = new JFrame("JEditorPane Test");
        myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myframe.setSize(400, 200);

        JEditorPane mypane = new JEditorPane();
        mypane.setContentType("text/plain");
        mypane.setText("Sleeping is necessary for a healthy body."
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days.");

        myframe.setContentPane(mypane);
        myframe.setVisible(true);

    }
}
