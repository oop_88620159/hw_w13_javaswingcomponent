/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;

import com.sun.jdi.Value;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author acer
 */
public class JavaJPasswordField {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        frame.add(value);

        JLabel lbl = new JLabel("Password.");
        lbl.setBounds(20, 100, 80, 30);
        frame.add(lbl);

    }
}
