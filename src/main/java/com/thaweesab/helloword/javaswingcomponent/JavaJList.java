/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;

/**
 *
 * @author acer
 */
public class JavaJList {
    public static void main(String[] args) {
        
        JFrame frame = new JFrame();
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        
        DefaultListModel<String> l1 = new DefaultListModel<>();
        l1.addElement("Item1");
        l1.addElement("Item2");
        l1.addElement("Item3");
        l1.addElement("Item4");
        
        JList<String> list = new JList<>(l1);
        list.setBounds(100, 100, 75, 75);
        frame.add(list);
        
    }
}
