/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author acer
 */
public class JavaJTable {

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        frame.setSize(300, 400);
        frame.setVisible(true);

        String data[][] = {{"101", "Amit", "670000"},
        {"102", "Jai", "780000"},
        {"101", "Sachin", "700000"}};

        String column[] = {"ID", "NAME", "SALARY"};

        JTable tb = new JTable(data, column);
        tb.setBounds(30, 40, 200, 300);
        frame.add(tb);

        JScrollPane sp = new JScrollPane(tb);
        frame.add(sp);
    }
}

