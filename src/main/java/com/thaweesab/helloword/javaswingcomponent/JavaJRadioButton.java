/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import javax.swing.JFrame;
import javax.swing.JRadioButton;


/**
 *
 * @author acer
 */
public class JavaJRadioButton {

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

        JRadioButton rbtn1 = new JRadioButton("A) Male");
        rbtn1.setBounds(75, 50, 100, 30);
        frame.add(rbtn1);

        JRadioButton rbtn2 = new JRadioButton("B) Female");
        rbtn2.setBounds(75, 100, 100, 30);
        frame.add(rbtn2);

    }
}

