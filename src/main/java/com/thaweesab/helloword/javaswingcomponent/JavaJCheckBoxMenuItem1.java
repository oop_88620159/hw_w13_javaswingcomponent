/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author acer
 */
public class JavaJCheckBoxMenuItem1 {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Jmenu Exmple");
        frame.setSize(350, 250);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        
        JMenuBar menubar = new JMenuBar();
        // File Menu, F - Mnemonic  //
        JMenu filemenu = new JMenu("File");
        filemenu.setMnemonic(KeyEvent.VK_F);
        menubar.add(filemenu);
        // File->New, N - Mnemonic  
        JMenuItem menultem1 = new JMenuItem("Open", KeyEvent.VK_N);
        filemenu.add(menultem1);
        
        JCheckBoxMenuItem casemenultem = new JCheckBoxMenuItem("Option_1");
        casemenultem.setMnemonic(KeyEvent.VK_C);
        filemenu.add(casemenultem);
       
        
        ActionListener alistener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                AbstractButton abutton = (AbstractButton) event.getSource();
                boolean selected = abutton.getModel().isSelected();
                String newlabel;
                Icon newicon;                
                if (selected) {
                    newlabel = "Value-1";
                } else {
                    newlabel = "Value-2";
                }
                abutton.setText(newlabel);
            }
        };
        casemenultem.addActionListener(alistener);
        frame.setJMenuBar(menubar);
    }
}
