/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author acer
 */
public class JavaJSeparator {

    public static void main(String[] args) {

        JMenu menu, submenu;
        JMenuItem i1, i2, i3, i4, i5;

        JFrame frame = new JFrame("Separator Example");
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);

        JMenuBar mb = new JMenuBar();

        menu = new JMenu("Menu");
        i1 = new JMenuItem("Item 1");
        i2 = new JMenuItem("Item 2");
        menu.add(i1);
        menu.addSeparator();
        menu.add(i2);
        mb.add(menu);

        frame.setJMenuBar(mb);

    }
}

