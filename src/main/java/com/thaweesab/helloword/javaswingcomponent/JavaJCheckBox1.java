/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;

import javax.swing.JCheckBox;
import javax.swing.JFrame;

/**
 *
 * @author acer
 */
public class JavaJCheckBox1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("CheckBox Examlp");
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        
        
        
        JCheckBox checkBox1 = new JCheckBox("C++");
        checkBox1.setBounds(100, 100, 50, 50);
        frame.add(checkBox1);
        
        
        
        JCheckBox checkBox2 = new JCheckBox("Java", true);
        checkBox2.setBounds(100, 150, 100, 50);
        frame.add(checkBox2);
        
    }
}


