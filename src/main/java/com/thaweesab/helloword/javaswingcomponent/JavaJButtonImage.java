/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author acer
 */
public class JavaJButtonImage {

    public static void main(String[] args) {

        JFrame frame = new JFrame("Button Example");
        frame.setSize(300, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        
        
        JButton btnimage = new JButton(new ImageIcon("D:\\icon.png"));
        btnimage.setBounds(100, 100, 100, 40);
        frame.add(btnimage);

    }
}


