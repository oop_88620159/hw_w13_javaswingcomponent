/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 *
 * @author acer
 */
class MyJComponent extends JComponent {

    public void paint(Graphics g) {
        g.setColor(Color.green);
        g.fillRect(300, 300, 100, 100);
    }
}

public class JavaJComponent {

    public static void main(String[] args) {

        MyJComponent com = new MyJComponent();
        
        JFrame frame = new JFrame("JComponent Example");
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frame.add(com);
        frame.setVisible(true);
       
    }

}

