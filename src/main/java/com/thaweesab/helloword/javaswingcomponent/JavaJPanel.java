/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author acer
 */
public class JavaJPanel {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Panel Example");
        frame.setSize(400, 400);
        frame.setLayout(null);//
        frame.setVisible(true);

        JPanel panel = new JPanel();
        panel.setBounds(40, 80, 200, 200);
        panel.setBackground(Color.gray);
        frame.add(panel);

        
        JButton btn1 = new JButton("Button 1");
        btn1.setBounds(50, 100, 80, 30);
        btn1.setBackground(Color.yellow);
        JButton btn2 = new JButton("Button 2");
        btn2.setBounds(100, 100, 80, 30);
        btn2.setBackground(Color.green);
        panel.add(btn1);
        panel.add(btn2);
    }

}
