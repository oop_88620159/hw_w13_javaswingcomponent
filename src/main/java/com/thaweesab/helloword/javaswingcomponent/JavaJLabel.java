/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import javax.swing.JFrame;
import javax.swing.JLabel;


/**
 *
 * @author acer
 */
public class JavaJLabel {

    public static void main(String[] args) {

        JFrame frame = new JFrame("Label Example"); 
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

        JLabel lblfirst = new JLabel("First Label."); 
        lblfirst.setBounds(50, 50, 100, 30);
        frame.add(lblfirst);

        JLabel lblsecond = new JLabel("Second Label.");
        lblsecond.setBounds(50, 100, 100, 30);
        frame.add(lblsecond);

    }
}

