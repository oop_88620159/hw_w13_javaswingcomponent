/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JTextField;


/**
 *
 * @author acer
 */
public class JavaJTextField {

    public static void main(String[] args) {
        JTextField txt1, txt2;

        JFrame frame = new JFrame("TextField Example");
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);

        txt1 = new JTextField("Welcome to Javatpoint.");
        txt1.setBounds(50, 100, 200, 30);
        frame.add(txt1);

        txt2 = new JTextField("AWT Tutorial");
        txt2.setBounds(50, 150, 200, 30);
        frame.add(txt2);
    }

}
