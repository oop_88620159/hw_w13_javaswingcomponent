/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.Border;

/**
 *
 * @author acer
 */
public class JavaJToolBar {

    public static void main(String[] args) {
        JFrame myframe = new JFrame("JToolBar Example");
        myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myframe.setSize(450, 250);
        myframe.setVisible(true);

        
        JToolBar toolbar = new JToolBar();
        toolbar.setRollover(true);

        
        JButton button = new JButton("File");
        toolbar.add(button);
        toolbar.addSeparator();
        toolbar.add(new JButton("Edit"));
        toolbar.add(new JComboBox(new String[]{"Opt-1", "Opt-2", "Opt-3", "Opt-4"}));

        Container contentpane = myframe.getContentPane();
        contentpane.add(toolbar, BorderLayout.NORTH);

        
        JTextArea txtarea = new JTextArea();
        JScrollPane mypane = new JScrollPane(txtarea);

        contentpane.add(mypane, BorderLayout.EAST);
    }

}

