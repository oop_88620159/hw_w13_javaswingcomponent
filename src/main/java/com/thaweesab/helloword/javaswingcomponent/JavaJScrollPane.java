/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author acer
 */
public class JavaJScrollPane {

    private static final long serialVersionUID = 1L;

    private static void createAndShowGUI() {

        // Create and set up the window.  
        final JFrame frame = new JFrame("Scroll Pane Example");

        // Display the window. 
        frame.setSize(500, 500);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        
        
        // set flow layout for the frame  
        frame.getContentPane().setLayout(new FlowLayout());

        JTextArea txtarea = new JTextArea(20, 20);

        JScrollPane scollabletextarea = new JScrollPane(txtarea);

        
        
        scollabletextarea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scollabletextarea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        frame.getContentPane().add(scollabletextarea);
    }

    
    
    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                createAndShowGUI();
            }
        });
    }
}

