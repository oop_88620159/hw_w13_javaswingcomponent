/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author acer
 */
public class JavaJSpinner {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Spinner Example");
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

        SpinnerModel value = new SpinnerNumberModel(5, //initial value 
                0, //minimum value  
                10, //maximum value  
                1); //step  

        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100, 100, 50, 30);
        frame.add(spinner);
    }

}
