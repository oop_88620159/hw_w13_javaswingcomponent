/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author acer
 */
public class JavaJTextPane {

    public static void main(String[] args) throws BadLocationException {
        JFrame frame = new JFrame("JTextPane Example");
        frame.setSize(400, 300);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container cp = frame.getContentPane();
        
        
        JTextPane pane = new JTextPane();
        SimpleAttributeSet attributeset = new SimpleAttributeSet();
        StyleConstants.setBold(attributeset, true);

        // Set the attributes before adding text  
        pane.setCharacterAttributes(attributeset, true);
        pane.setText("Welcome");

        attributeset = new SimpleAttributeSet();
        StyleConstants.setItalic(attributeset, true);
        StyleConstants.setForeground(attributeset, Color.red);
        StyleConstants.setBackground(attributeset, Color.blue);

        Document doc = pane.getStyledDocument();
        doc.insertString(doc.getLength(), "To Java ", attributeset);

        attributeset = new SimpleAttributeSet();
        doc.insertString(doc.getLength(), "World", attributeset);

        JScrollPane scrollPane = new JScrollPane(pane);
        cp.add(scrollPane, BorderLayout.CENTER);
    }

}

