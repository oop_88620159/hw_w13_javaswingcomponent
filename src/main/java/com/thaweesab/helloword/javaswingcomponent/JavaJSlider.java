/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 *
 * @author acer
 */
public class JavaJSlider extends JFrame {

    public JavaJSlider() {

        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);

        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);

    }

    
    
    public static void main(String[] args) {
        JavaJSlider frame = new JavaJSlider();
        frame.pack();
        frame.setVisible(true);

    }
}
