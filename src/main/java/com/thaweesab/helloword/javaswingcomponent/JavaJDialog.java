/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.javaswingcomponent;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author acer
 */
public class JavaJDialog {

    private static JDialog d;

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        
        d = new JDialog(frame, "Dialog Examp;e", true);
        d.setLayout(new FlowLayout());

        
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JavaJDialog.d.setVisible(false);
            }

        });

        
        d.add(new JLabel("Click button to continue"));
        d.add(btn);
        d.setSize(300, 300);
        d.setVisible(true);

    }

}
